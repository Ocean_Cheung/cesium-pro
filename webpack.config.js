const { resolve } = require('path')

module.exports = {
    mode: 'production',
    entry: './src/plugins/index.js',
    output: {
        filename: 'Ocesium.min.js',
        path: resolve(__dirname, 'build'),
        library: 'OCesium',
        libraryTarget: 'commonjs'
    },
    resolve: {
        extensions: ['*', '.js', '.vue'],
        alias: {
            '@': resolve('src')
        }
    },
    module: {
        rules: [{
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'less-loader'
                ]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                exclude: /\.(css|js|html|png|jpg|gif|less)/,
                loader: 'file-loader',
                options: {
                    name: '[hash:10].[ext]'
                }
            }
        ]
    },
    plugins: [

    ],
}