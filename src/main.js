import Vue from 'vue'
import App from './App.vue'
import router from './router'
// import store from './store'

Vue.config.productionTip = false;

import dataV from "@jiaminghi/data-view";
Vue.use(dataV);

// import 'lib-flexible';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

import VueJsonp from 'vue-jsonp';
Vue.use(VueJsonp);

import axios from "axios";
Vue.prototype.$axios = axios;


import * as OCesium from "./plugins/index";
// import { OCesium } from "../build/Ocesium.min";
import './plugins/index.css';
window.OCesium = OCesium;


new Vue({
    router,
    render: h => h(App)
}).$mount('#app')