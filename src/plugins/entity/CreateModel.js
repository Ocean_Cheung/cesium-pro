class CreateModel {
    constructor(url, position, orientation) {
        return this._init(url, position, orientation);
    }
    _init(url, position, orientation) {
        return {
            name: url,
            position: position,
            orientation: orientation,
            model: {
                uri: url,
                minimumPixelSize: 128,
                maximumScale: 20000
            }
        };
    }
}
export default CreateModel;