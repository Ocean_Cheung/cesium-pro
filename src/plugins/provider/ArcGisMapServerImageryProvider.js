class ArcGisMapServerImageryProvider {
    constructor(url, options) {
        this.defaultOptions = {
            url: url
        };
        return this._init(url, options);
    }
    _init(url, options) {
        if (options !== undefined) {
            Object.assign(this.defaultOptions, options);
        }
        return new Cesium.ArcGisMapServerImageryProvider(this.defaultOptions);

    }
}
export default ArcGisMapServerImageryProvider;