class EntityUtil {
    static resetEntity(viewer) {
        for (let i = 0, j = viewer.entities.values.length; i < j; i++) {
            viewer.entities.values[i].model.color = null;
        }
    }
    static getEntityById(viewer, id) {
        return viewer.entities.values.length.filter(item => item.id = id)[0];
    }
}

export default EntityUtil;