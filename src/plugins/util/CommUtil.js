import { v4 as uuidv4 } from 'uuid';
class CommUtil {
    static createUuid() {
        return uuidv4();
    }
}
export default CommUtil;