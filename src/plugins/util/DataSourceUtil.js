class DataSourceUtil {
    static createDataSource(name) {
        return new Cesium.CustomDataSource(name);
    }
    static getDataSourceByName(viewer, name) {
        return viewer.dataSources._dataSources.filter(item => item.name === name)[0];
    }
}
export default DataSourceUtil;