class LocateUtil {
    /**
     * 
     * @param {视图} viewer 
     * @param {*坐标点或者范围} destination 
     * @param {*方向} orientation 
     */
    constructor(viewer, destination, orientation) {
        const { x, y, z } = destination;
        const { heading, pitch, roll } = orientation;
        this.x = x;
        this.y = y;
        this.z = z || 0.0;
        this.heading = heading;
        this.pitch = pitch;
        this.roll = roll || 0.0;
        this.viewer = viewer;
        if (Array.isArray(destination)) {
            this.rect = destination;
        }
    }
    viewerFlyToPoint() {
        const _this = this;
        if (_this.entity)
            viewer.entities.remove(_this.entity);
        //初始化实例
        _this.entity = new Cesium.Entity({
            id: 'flyTmp',
            position: Cesium.Cartesian3.fromDegrees(_this.x, _this.y),
            point: {
                pixelSize: 10,
                color: Cesium.Color.WHITE.withAlpha(0.9),
                outlineColor: Cesium.Color.WHITE.withAlpha(0.9),
                outlineWidth: 1
            }
        });
        _this.viewer.entities.add(entity);
        _this.viewer.flyTo(_this.entity, {
            duration: 5,
            offset: {
                heading: Cesium.Math.toRadians(_this.heading),
                pitch: Cesium.Math.toRadians(_this.pitch),
                range: _this.roll
            }
        });
    }
    viewerFlyToExtent() {
        const _this = this;
        _this.entity = new Cesium.Entity({
            id: 'locationRectangle',
            rectangle: {
                coordinates: _this.rect,
                material: Cesium.Color.GREEN.withAlpha(1.0),
                height: 10.0,
                outline: false
            }
        })
        _this.viewer.entities.add(_this.entity);
        viewer.flyTo(_this.entity, {
            duration: 5,
            offset: {
                heading: Cesium.Math.toRadians(_this.heading),
                pitch: Cesium.Math.toRadians(_this.pitch),
                range: _this.roll
            }
        });
    }
    cameraSetView() {
        const _this = this;
        _this.viewer.camera.setView({
            destination: Cesium.Cartesian3.fromDegrees(_this.x, _this.y, _this.z),
            orientation: {
                heading: Cesium.Math.toRadians(_this.heading), //指向
                pitch: Cesium.Math.toRadians(_this.pitch), //视角
                roll: _this.roll
            }
        });
    }
    cameraFlyToPoint() {
        const _this = this;
        _this.viewer.camera.flyTo({
            destination: Cesium.Cartesian3.fromDegrees(_this.x, _this.y, _this.z),
            orientation: {
                heading: Cesium.Math.toRadians(_this.heading), //指向
                pitch: Cesium.Math.toRadians(_this.pitch), //视角
                roll: _this.roll
            }
        });
    }
    cameraFlyToExtent() {
        const _this = this;
        _this.viewer.camera.flyTo({
            destination: _this.rect,
            duration: 5,
            orientation: {
                heading: Cesium.Math.toRadians(_this.heading), //指向
                pitch: Cesium.Math.toRadians(_this.pitch), //视角
                roll: _this.roll
            }
        });
    }
    static zoomToEntity(viewer, entity) {
        viewer.zoomTo(entity);
    }
}