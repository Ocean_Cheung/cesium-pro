class MouseUtil {
    static getClickPosition(viewer, event, type = 1) {
        return new Promise(resolve => {
            let position;
            if (type == 1) { //获取鼠标点的对应椭球面位置
                position = viewer.scene.camera.pickEllipsoid(event.position, viewer.scene.globe.ellipsoid);
            } else if (type == 2) { //获取加载地形后对应的经纬度和高程：地标坐标 即世界坐标
                let ray = viewer.camera.getPickRay(event.position);
                position = viewer.scene.globe.pick(ray, viewer.scene);
            } else if (type == 3) { // 获取倾斜摄影或模型点击处的坐标：场景坐标
                //解决viewer.scene.pickPosition(e.position)在没有3dTile模型下的笛卡尔坐标不准问题
                viewer.scene.globe.depthTestAgainstTerrain = true; //默认为false
                position = viewer.scene.pickPosition(event.position);
            } else if (type == 4) { //获取点击处屏幕坐标 ：屏幕坐标（鼠标点击位置距离canvas左上角的像素值）
                position = event.position;
            }
            resolve(position);
        })
    }
}
export default MouseUtil;
/**参考博客：https://blog.csdn.net/qq_38870665/article/details/112299972 */