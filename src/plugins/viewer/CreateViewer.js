import CesiumNavigation from 'es6-cesium-navigation';
class CreateViewer {
    constructor($el, viewerOption, navigationOption) {
        this.cesiumInstance = {};
        this.defaultViewerOption = {
            geocoder: false, // 地理位置查询定位控件
            homeButton: false, // 默认相机位置控件
            timeline: false, // 时间滚动条控件
            navigationHelpButton: false, // 默认的相机控制提示控件
            fullscreenButton: false, // 全屏控件
            scene3DOnly: true, // 每个几何实例仅以3D渲染以节省GPU内存
            baseLayerPicker: false, // 底图切换控件
            animation: false, // 控制场景动画的播放速度控件
            selectionIndicator: false,
            infoBox: false
        };
        this.defaultNavigationOption = {
            // 用于在使用重置导航重置地图视图时设置默认视图控制。接受的值是经纬度数组[lon,lat,height]或者 rectangle[west,south,east,north]
            defaultResetView: [119.782, 31.740, 15000.0],
            // 用于启用或禁用罗盘。true是启用罗盘，false是禁用罗盘。默认值为true。如果将选项设置为false，则罗盘将不会添加到地图中。
            enableCompass: true,
            // 用于启用或禁用缩放控件。true是启用，false是禁用。默认值为true。如果将选项设置为false，则缩放控件将不会添加到地图中。
            enableZoomControls: true,
            // 用于启用或禁用距离图例。true是启用，false是禁用。默认值为true。如果将选项设置为false，距离图例将不会添加到地图中。
            enableDistanceLegend: true,
            // 用于启用或禁用指南针外环。true是启用，false是禁用。默认值为true。如果将选项设置为false，则该环将可见但无效。
            enableCompassOuterRing: true
        };
        return this._init($el, viewerOption, navigationOption);
    }
    _init($el, viewerOption, navigationOption) {
        if (Object.prototype.toString.call(viewerOption) === '[object Object]' && JSON.stringify(viewerOption) !== "{}") {
            Object.assign(this.defaultViewerOption, viewerOption);
        }
        if (Object.prototype.toString.call(navigationOption) === '[object Object]' && JSON.stringify(navigationOption) !== "{}") {
            Object.assign(this.defaultNavigationOption, navigationOption);
        }
        let viewer = new Cesium.Viewer($el, this.defaultViewerOption);
        viewer._cesiumWidget._creditContainer.style.display = "none";
        this.cesiumInstance["viewer"] = viewer;
        //兼容两种模式
        if (navigationOption === undefined || navigationOption === "" || navigationOption === null) {
            this.cesiumInstance["options"] = {};
        } else {
            this.cesiumInstance = new CesiumNavigation(viewer, this.defaultNavigationOption);
        }
        return this.cesiumInstance;
    }
}
export default CreateViewer;