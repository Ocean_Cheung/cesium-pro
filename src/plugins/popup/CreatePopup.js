import CommUtil from '@/plugins/util/CommUtil';
class CreatePopup {
    constructor(viewer, geometry, title = '', content = '') {
        this.viewer = viewer; //弹窗创建的viewer
        this.geometry = geometry; //弹窗挂载的位置
        this.preDom(title, content); //创建dom
        this.render(); //渲染dom
        this.addEvent(); //绑定事件
        this.removeDom(); //移除dom
    }
    preDom(title, content) {
        this.domId = CommUtil.createUuid();
        const template = '<div id=' + this.domId + ' class="oce-pop">' +
            '    <div class="oce-header">' +
            '        <div class="oce-title">' + title + '</div>' +
            '        <div id=' + `${this.domId}_close` + ' class="oce-close el-icon-close"></div>' +
            '    </div>' +
            '    <div class="oce-content">' +
            content +
            '    </div>' +
            '    <div class="oce-arrow"></div>' +
            '</div>';
        const popupDom = document.createRange().createContextualFragment(template);
        document.body.appendChild(popupDom);
        this.popupDom = document.getElementById(this.domId);
        this.closeDom = document.getElementById(`${this.domId}_close`);
    }
    render() {
        const _this = this;
        const position = Cesium.SceneTransforms.wgs84ToWindowCoordinates(_this.viewer.scene, _this.geometry);
        if (position) {
            _this.popupDom.style.left = (position.x - _this.popupDom.offsetWidth / 2) + 'px';
            _this.popupDom.style.top = (position.y - _this.popupDom.offsetHeight - 20) + 'px';
        }
    }
    addEvent() {
        const _this = this;
        _this.eventListener = _this.viewer.clock.onTick.addEventListener(clock => {
            _this.render(_this.geometry);
        })
    }
    removeDom() {
        const _this = this;
        _this.closeDom.addEventListener('click', event => {
            document.body.removeChild(this.popupDom);
            _this.viewer.clock.onTick.removeEventListener(_this.eventListener);
        })
    }
};
export default CreatePopup;